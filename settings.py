"""
Contains class distribution problem settings
"""
# Available pupil levels
levels = [1, 2, 3]

# Maximum number of iterations
HILLCLIMBING_MAX_ITERATIONS = 1000

# RULE WEIGHTS
# Importance of the Boy/Girl distribution rule
RULE_BOYGIRL_WEIGHT = 10

# Importance of the group capacity rule
RULE_PUPILSCAPACITY_WEIGHT = 750
# Penalty for going over capacity on a group
RULE_PUPILSCAPACITY_OVERCAP_PENALTY = 1000

# Importance of the natives/non-natives distribution rule
RULE_NATIVES_WEIGHT = 15

# Importance of the gon/non-gon distribution rule
RULE_GONS_WEIGHT = 100

# Importance of the levels distribution rules
RULE_LEVELS_WEIGHT = 10

# Importance of the friends distribution rule
RULE_FRIENDS_WEIGHT = 50