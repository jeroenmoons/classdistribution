*ClassDistribution*

ClassDistribution is an Open Source project aimed at solving a very common problem in schools.

Every year, all pupils going to the next grade need to be distributed over a number of groups. This distribution needs to take into account a lot of factors:

Gender of the pupils
Level of the pupils
Nationality
Make sure every pupil has at least a few friends in his group
Make sure pupils who cause problems together to be in separate groups
...
Since it takes teachers huge amounts of time to come up with an acceptable distribution this project was brought into existence. It aims to solve this problem by providing a platform in which the user (teacher) can input a list of students and their properties and a number of groups (+capacity), for which an optimal solution will be searched.

A total exploration of the state space is near impossible since the number of possible configurations is huge. The current approach is to make a random initial distribution and do a hill-climbing search to find the best solution. If it turns out this is not feasible we might have to turn to other approaches (genetic search, ...)

This software is completely free for any non-commercial use whatsoever.
