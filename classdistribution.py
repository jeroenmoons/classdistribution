"""
Script to find an optimal assignment of pupils to a number of groups
based on their properties (name, nationality, level, friends, ...)

@todo: Implement other criteria (friends, enemies)
"""

import yaml
from problem import ClassDistributionProblem
import search, time
from pprint import pprint

if __name__ == '__main__':
    # Load config from yaml file
    configFile = open('naarteerste.yml', 'r')
    config = yaml.load(configFile)

    groups = config['groups']
    pupils = config['pupils']
    
    # Time the execution
    start = time.time()

    # Create Problem object
    problem = ClassDistributionProblem(groups, pupils)
    
    print "Initial problem: " 
    problem.display(problem.initial)
    
    # Do a hill climbing search on problem
    solutionNode = search.hillclimbingSearch(problem)
    
    # End time
    end = time.time() - start
    
    # Output the final state
    print "-" * 25
    print "Solution: "
    problem.display(solutionNode.state)
    print "-" * 25
    print "Found solution in ", solutionNode.path_cost, " steps. Took ", end, " seconds" 