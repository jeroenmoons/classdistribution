'''
Contains Search functionality
'''
from pprint import pprint
from utils import update, argmax_random_tie
import settings

class Node:
    """A node in a search tree. Contains a pointer to the parent (the node
    that this is a successor of) and to the actual state for this node. Note
    that if a state is arrived at by two paths, then there are two nodes with
    the same state.  Also includes the action that got us to this state, and
    the total path_cost (also known as g) to reach the node.  Other functions
    may add an f and h value; see best_first_graph_search and astar_search for
    an explanation of how the f and h values are handled. You will not need to
    subclass this class."""

    def __init__(self, state, parent=None, action=None, path_cost=0):
        "Create a search tree Node, derived from a parent by an action."
        update(self, state=state, parent=parent, action=action,
               path_cost=path_cost, depth=0)
        if parent:
            self.depth = parent.depth + 1
 
    def __repr__(self):
        return "<Node %s>" % (self.state,)
 
    def expand(self, problem):
        "List the nodes reachable in one step from this node."
        return [self.child_node(problem, action)
                for action in problem.actions(self.state)]
 
    def child_node(self, problem, action):
        childNode = problem.result(self.state, action)
        return Node(childNode, self, action, 
                    problem.path_cost(self.path_cost, self.state, action, next))


def hillclimbingSearch(problem):
    """Do a hill climbing search for a problem"""
    
    print "Starting hill climbing search"
    
    # Create a Node for the initial state
    node = Node(problem.initial)
    
    print "initial node is ", node
    
    # Put a cap on the number of iterations
    maxIterations = settings.HILLCLIMBING_MAX_ITERATIONS = 1000
    it = 0
    
    # Until an optimal distribution is found or we run into a preset limit    
    while (True and it < maxIterations):
        print "-" * 25
        print "search iteration ", (it + 1)
        print "current node is ", node
        
        # Find neighbors
        neighbors = node.expand(problem)
        
        print "Found ", len(neighbors), " neighbors"

        # No neighbors found -> can't look any further
        if not neighbors:
            break
        
        # For debugging purposes, print all found neighbors
        nr = 1
        for neighbor in neighbors:
            # print "value of neighbor node ", nr, " ", neighbor, "is ", problem.value(neighbor.state)
            nr += 1

        # Find the neighbor with the highest value
        neighbor = argmax_random_tie(neighbors,
                                     lambda aNode: problem.value(aNode.state))
        
        print "highest value neighbor: ", neighbor, ": ", problem.value(neighbor.state)
        
        # If no neighbor has a higher value than the current best node, stop
        if problem.value(neighbor.state) <= problem.value(node.state):
            break
        
        # Best neighbor becomes the current node
        node = neighbor
        
        # Next iteration
        it += 1
        
    # Return the node with the highest value we could find
    return node
