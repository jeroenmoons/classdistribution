"""
Contains classes defining the Class Distribution problem
"""
from pprint import pprint
import settings, itertools, math


class ClassDistributionProblem:
    """Represent the problem of distributing a number of Pupils over a number of
    Groups. 
    """
    def __init__(self, groups, pupils):
        """Initialize the problem using the configuration received by the 'groups'
        and 'pupils' parameters
        """
        self.groups = groups

        self.pupils = self.createPupilObjects(pupils)

        self.initial = self.setInitialState()
        self.state = self.initial

        self.groupCombos = []
        for groupA, groupB in itertools.combinations(self.groups.keys(), 2):
            self.groupCombos.append((groupA, groupB))

    def createPupilObjects(self, pupils):
        """Create Pupil objects for each pupil found in the 'pupils' dict
        """
        pupilObjects = {}

        for pupilId in pupils.keys():
            pupilData = pupils[pupilId]

            pupil = Pupil()
            pupil.id = pupilId
            pupil.boy = pupilData.get('boy', True)
            pupil.name = pupilData.get('name', 'Anonymous')
            pupil.level = pupilData.get('level', 1)
            pupil.native = pupilData.get('native', True)
            pupil.gon = pupilData.get('gon', False)
            pupil.friends = pupilData.get('friends', [])

            pupilObjects[pupilId] = pupil

        return pupilObjects

    def setInitialState(self):
        """Takes the list of pupil objects and creates an initial distribution
        without taking any of their properties into account
        """
        initState = []
        groupIndex = 0
        groupKeys = self.groups.keys()
        nrOfGroups = len(groupKeys)

        for pupilId in self.pupils:
            initState.append((pupilId, groupKeys[groupIndex]))

            # Take next group for next pupil
            groupIndex += 1
            groupIndex %= nrOfGroups

        return initState

    def actions(self, state):
        """List of possible action to take in state 'state': Each of the pupil
        objects can be moved to one of the groups he's not belonging to according
        to 'state'.
        """
        actions = []

        for assignment in state:
            pupil = self.pupils[assignment[0]]

            otherGroups = [x for x in self.groups.keys() if x not in [assignment[1]]]

            for groupName in otherGroups:
                newAssignment = pupil.id, groupName
                actions.append(newAssignment)

        return actions

    def result(self, state, action):
        """Return the result of 'action' on 'state'
        """
        # Copy the state, but create a new tuple for the pupil specified by 
        # 'action' according to 'action'.
        newState = []
        for assignment in state:
            if action[0] == assignment[0]:
                # Create a new tuple for the assignment
                reassignment = assignment[0], action[1]
                newState.append(reassignment)
            else:
                # Keep the assignment unchanged
                newState.append(assignment)

        return newState

    def path_cost(self, cost_so_far, A, action, B):
        """Return path cost for taking action 'action' on state 'A', resulting 
        in state 'B'
        """
        return cost_so_far + 1

    def value(self, state):
        """The core of the solution, heuristic function determining the value
        of a state. The hill climbing search will try to maximize the value
        this method outputs. Each of the criteria adds penalty points (negative
        values) to the final value. 0 is the highest attainable value.
        """
        value = 0

        # The boys/girls ratios of the groups should be as close to each other
        # as possible
        value -= self.boyGirlRatioValue(state)

        # Groups should hold a number of pupils in relation to their capacity
        # No group should be over capacity (penalty on this is set in settings)
        value -= self.pupilsCapacityRatioValue(state)

        # The foreign/native ratios of the groups should be as close to each
        # other as possible
        value -= self.nativesRatioValue(state)

        # The gon/not gon ratios of the groups should be as close to each other
        # as possible
        value -= self.gonRatioValue(state)

        # Each group should have about equal numbers of each pupil level 
        value -= self.levelsRatioValue(state)

        # Each pupil should have as many friends in his group as possible
        value += self.friendsValue(state)

        # Each pupil should have as little enemies in his group as possible

        return value

    def boyGirlRatioValue(self, state):
        """Determine how well 'state' scores on the boy/girl ratio front. Each 
        group should have a boy/girl ratio that is as close as possible to the
        ratio of the other groups, so we punish differences in nr of boys
        """
        value = 0

        # Find nr of boys/girls per group
        boysPerGroup = self.nrOfBoysPerGroup(state)
        pupilsPerGroup = self.nrOfPupilsPerGroup(state)

        ratioPerGroup = {}

        for group in self.groups.keys():
            boys = boysPerGroup.get(group, 0)
            pupils = pupilsPerGroup.get(group, 1)

            ratioPerGroup[group] = float(boys)/pupils

        # For each combination of groups, calculate the difference in nr of boys
        # Subtract difference from value, multiply boy RULE_BOYGIRL_WEIGHT
        for groupA, groupB in self.groupCombos:
            diff = math.fabs(ratioPerGroup[groupA] - ratioPerGroup[groupB])
            # Use 1+diff so we get a value larger than 1 (so the weight will have
            # the effect of increasing rather than decreasing the weigthed diff)
            value += (1 + diff) * settings.RULE_BOYGIRL_WEIGHT

        return value

    def nrOfBoys(self):
        """Counts the number of male pupils"""
        boysOnly = [pupil.id for pupil in self.pupils.values() if pupil.boy]
        return len(boysOnly)

    def nrOfGirls(self):
        """Counts the number of female pupils"""
        girlsOnly = [pupil.id for pupil in self.pupils.values() if not pupil.boy]
        return len(girlsOnly)

    def nrOfBoysPerGroup(self, state):
        """Returns the number of boys in each group in state 'state'
        """
        boysPerGroup = {}

        for pupilId, groupName in state:
            if groupName in boysPerGroup and self.pupils[pupilId].boy:
                boysPerGroup[groupName] += 1
            elif self.pupils[pupilId].boy:
                boysPerGroup[groupName] = 1

        return boysPerGroup

    def nrOfGirlsPerGroup(self, state):
        """Returns the number of girls in the given group in state 'state'
        """
        girlsPerGroup = {}

        for pupilId, groupName in state:
            if groupName in girlsPerGroup and not self.pupils[pupilId].boy:
                girlsPerGroup[groupName] += 1
            elif not self.pupils[pupilId].boy:
                girlsPerGroup[groupName] = 1

        return girlsPerGroup

    def pupilsCapacityRatioValue(self, state):
        """Determine how well 'state' scores in the pupils/capacity ratio dept.
        """
        value = 0

        ratioPerGroup = {}
        pupilsPerGroup = self.nrOfPupilsPerGroup(state)

        for groupName, cap in self.groups.items():
            ratio = float(pupilsPerGroup[groupName])/cap
            ratioPerGroup[groupName] = ratio

            # Severe penalty for groups containing more pupils than they should
            if pupilsPerGroup[groupName] > cap:
                value += settings.RULE_PUPILSCAPACITY_OVERCAP_PENALTY

        for groupA, groupB in self.groupCombos:
            diff = math.fabs(ratioPerGroup[groupA] - ratioPerGroup[groupB])
            # Use 1+diff so we get a value larger than 1 (so the weight will have
            # the effect of increasing rather than decreasing the weigthed diff)
            value += (1 + diff) * settings.RULE_PUPILSCAPACITY_WEIGHT

        return value

    def nrOfPupilsPerGroup(self, state):
        """Return a dict with the number of pupils for each group in 'state'
        """
        nrPerGroup = {}

        for pupilId, groupName in state:
            if groupName not in nrPerGroup:
                nrPerGroup[groupName] = 0

            nrPerGroup[groupName] += 1

        return nrPerGroup

    def nativesRatioValue(self, state):
        """Determine how well 'state' scores on the even distribution of native
        vs non-native pupils
        """
        value = 0

        # Find nr of natives/non-natives per group
        nativesPerGroup = self.nrOfNativesPerGroup(state)
        pupilsPerGroup = self.nrOfPupilsPerGroup(state)

        ratioPerGroup = {}

        for group in self.groups.keys():
            natives = nativesPerGroup.get(group, 0)
            pupils = pupilsPerGroup.get(group, 1)

            ratioPerGroup[group] = float(natives)/pupils

        # For each combination of groups, calculate the difference in nr of natives
        # Subtract difference from value, multiply boy RULE_BOYGIRL_WEIGHT
        for groupA, groupB in self.groupCombos:
            diff = math.fabs(ratioPerGroup[groupA] - ratioPerGroup[groupB])
            # Use 1+diff so we get a value larger than 1 (so the weight will have
            # the effect of increasing rather than decreasing the weigthed diff)
            value += (1 + diff) * settings.RULE_NATIVES_WEIGHT

        return value

    def nrOfNatives(self):
        """Calculate the number of native pupils in the problem
        """
        nativesOnly = [pupil.id for pupil in self.pupils.values() if pupil.native]
        return len(nativesOnly)

    def nrOfNonNatives(self):
        """Calculate the nubmer of non-native pupils in the problem
        """
        nonNativesOnly = [pupil.id for pupil in self.pupils.values() if not pupil.native]
        return len(nonNativesOnly)

    def nrOfNativesPerGroup(self, state):
        """Return the number of natives per group in 'state'
        """
        nativesPerGroup = {}

        for pupilId, groupName in state:
            if groupName in nativesPerGroup and self.pupils[pupilId].native:
                nativesPerGroup[groupName] += 1
            elif self.pupils[pupilId].native:
                nativesPerGroup[groupName] = 1

        return nativesPerGroup

    def nrOfNonNativesPerGroup(self, state):
        """Return the number of non-natives per group in 'state'
        """
        nnativesPerGroup = {}

        for pupilId, groupName in state:
            if groupName in nnativesPerGroup and not self.pupils[pupilId].native:
                nnativesPerGroup[groupName] += 1
            elif not self.pupils[pupilId].native:
                nnativesPerGroup[groupName] = 1

        return nnativesPerGroup

    def gonRatioValue(self, state):
        """Determine how well 'state' scores in the gon pupils distribution dept.
        """
        value = 0

        # Find nr of natives/non-natives per group
        gonsPerGroup = self.nrOfGonsPerGroup(state)
        pupilsPerGroup = self.nrOfPupilsPerGroup(state)

        ratioPerGroup = {}

        for group in self.groups.keys():
            gons = gonsPerGroup.get(group, 0)
            pupils = pupilsPerGroup.get(group, 1)

            ratioPerGroup[group] = float(gons)/pupils

        # For each combination of groups, calculate the difference in nr of gons
        # Subtract difference from value, multiply boy RULE_BOYGIRL_WEIGHT
        for groupA, groupB in self.groupCombos:
            diff = math.fabs(ratioPerGroup[groupA] - ratioPerGroup[groupB])
            # Use 1+diff so we get a value larger than 1 (so the weight will have
            # the effect of increasing rather than decreasing the weigthed diff)
            value += (1 + diff) * settings.RULE_GONS_WEIGHT

        return value

    def nrOfGons(self):
        """Calculate the number of gon pupils in the problem
        """
        gonsOnly = [pupil.id for pupil in self.pupils.values() if pupil.gon]
        return len(gonsOnly)

    def nrOfNonGons(self):
        """Calculate the number of non-gon pupils in the problem
        """
        nonGonsOnly = [pupil.id for pupil in self.pupils.values() if not pupil.gon]
        return len(nonGonsOnly)

    def nrOfGonsPerGroup(self, state):
        """Return the number of gons per group in 'state'
        """
        gonsPerGroup = {}

        for pupilId, groupName in state:
            if groupName in gonsPerGroup and self.pupils[pupilId].gon:
                gonsPerGroup[groupName] += 1
            elif self.pupils[pupilId].gon:
                gonsPerGroup[groupName] = 1

        return gonsPerGroup

    def nrOfNonGonsPerGroup(self, state):
        """Return the number of non-gons per group
        """
        nonGonsPerGroup = {}

        for pupilId, groupName in state:
            if groupName in nonGonsPerGroup and not self.pupils[pupilId].gon:
                nonGonsPerGroup[groupName] += 1
            elif not self.pupils[pupilId].gon:
                nonGonsPerGroup[groupName] = 1

        return nonGonsPerGroup

    def levelsRatioValue(self, state):
        """Determine how well 'state' scores at distributing pupils of each
        level over the available groups
        """
        value = 0

        for level in settings.levels:
            # Find nr of natives/non-natives per group
            levelXsPerGroup = self.nrOfLevelXsPerGroup(level, state)
            pupilsPerGroup = self.nrOfPupilsPerGroup(state)

            ratioPerGroup = {}

            for group in self.groups.keys():
                levelXs = levelXsPerGroup.get(group, 0)
                pupils = pupilsPerGroup.get(group, 1)
                ratioPerGroup[group] = float(levelXs)/pupils

            # For each combination of groups, calculate the difference in nr of gons
            # Subtract difference from value, multiply boy RULE_BOYGIRL_WEIGHT
            for groupA, groupB in self.groupCombos:
                diff = math.fabs(ratioPerGroup[groupA] - ratioPerGroup[groupB])
                # Use 1+diff so we get a value larger than 1 (so the weight will have
                # the effect of increasing rather than decreasing the weigthed diff)
                value += (1 + diff) * settings.RULE_LEVELS_WEIGHT

        return value

    def nrOfLevelXs(self, level):
        """Return the number of level X pupils"""
        levelXsOnly = [pupil.id for pupil in self.pupils.values() if pupil.level == level]
        return len(levelXsOnly)

    def nrOfNonLevelXs(self, level):
        """Return the number of non-level X pupils"""
        levelXsOnly = [pupil.id for pupil in self.pupils.values() if pupil.level != level]
        return len(levelXsOnly)

    def nrOfLevelXsPerGroup(self, level, state):
        """Return the number of level X pupils per group
        """
        levelXsPerGroup = {}

        for pupilId, groupName in state:
            if groupName in levelXsPerGroup and self.pupils[pupilId].level == level:
                levelXsPerGroup[groupName] += 1
            elif self.pupils[pupilId].level == level:
                levelXsPerGroup[groupName] = 1

        return levelXsPerGroup

    def nrOfNonLevelXsPerGroup(self, level, state):
        """Return the number of non-level X pupils per group
        """
        nonLevelXsPerGroup = {}

        for pupilId, groupName in state:
            if groupName in nonLevelXsPerGroup and self.pupils[pupilId].level != level:
                nonLevelXsPerGroup[groupName] += 1
            elif self.pupils[pupilId].level != level:
                nonLevelXsPerGroup[groupName] = 1

        return nonLevelXsPerGroup

    def friendsValue(self, state):
        """Determine how well 'state' scores on the keeping together of pupils
        who are considered friends.
        """
        value = 0

        stateDict = dict(state)

        for (pupilId, group) in state:
            # For each of the pupil's friends that are in the same group, add a bonus
            for n, friend in enumerate(self.pupils[pupilId].friends, start=1):
                if stateDict[friend] == group:
                    # bonus is (rule weight)/(loop counter squared), which makes
                    # each subsequent friend in the same group more 'valuable'
                    # than the next.
                    value += float(settings.RULE_FRIENDS_WEIGHT)/n**3

        return value

    def countFriendsInGroup(self, pupilId, pupilGroup, state):
        """Count the number of friends a pupil has in his group according to
        'state'.
        """
        # Make a list of pupil's friends in the same group
        friends = self.pupils[pupilId].friends
        friendsInGroup = [pupil for (pupil, group)
                          in state
                          if group == pupilGroup and pupil in friends]

        return len(friendsInGroup)

    def display(self, state):
        """Print a representation of the problem in a given state to the output.
        """
        print " "
        print "Problem"
        print "-" * 25

        # print the groups
        # print the pupils
        print self.toString()

        # Print boys and girls stats and score
        self.displayBoysAndGirls(state)
        print "Rule Boy/Girl score: ", self.boyGirlRatioValue(state)

        # Print group capacity score
        print "Rule Pupil Capacity score: ", self.pupilsCapacityRatioValue(state)

        # Print natives stats and score
        self.displayNatives(state)
        print "Rule Natives score: ", self.nativesRatioValue(state)

        # Print gon stats and score
        self.displayGons(state)
        print "Rule Gons score: ", self.gonRatioValue(state)

        # Print level stats and score
        self.displayLevels(state)
        print "Rule Levels score: ", self.levelsRatioValue(state)

        # Print friends score
        print "Rule Friends score: ", self.friendsValue(state)

        # Print the groups and the pupils they contain
        self.displayDistribution(state)

        print " "

    def displayBoysAndGirls(self, state):
        """Print this problem's boys/girls stats to output
        """
        # print the number of boys
        print self.nrOfBoys(), " boys in this problem"

        # print the number of boys in each group
        boysPerGroup = self.nrOfBoysPerGroup(state)

        for group, nr in boysPerGroup.items():
            print "group ", group, " has ", nr, " boys"

        # print the number of girls
        print self.nrOfGirls(), " girls in this problem"

        # print the number of girls in each group
        girlsPerGroup = self.nrOfGirlsPerGroup(state)

        for group, nr in girlsPerGroup.items():
            print "group ", group, " has ", nr, " girls"

    def displayNatives(self, state):
        """Print natives stats to output
        """
        print self.nrOfNatives(), " natives in this problem"

        # Print the number of natives in each group
        nativesPerGroup = self.nrOfNativesPerGroup(state)

        for group, nr in nativesPerGroup.items():
            print "group ", group, " has ", nr, " natives"

        print self.nrOfNonNatives(), " non-natives in this problem"

        # Print the number of non-natives in each group
        nonNativesPerGroup = self.nrOfNonNativesPerGroup(state)

        for group, nr in nonNativesPerGroup.items():
            print "group ", group, " has ", nr, " non-natives"

    def displayGons(self, state):
        """Print gons stats to output
        """
        print self.nrOfGons(), " gons in this problem"

        # Print the number of gons in each group
        gonsPerGroup = self.nrOfGonsPerGroup(state)

        for group, nr in gonsPerGroup.items():
            print "group ", group, " has ", nr, " gons"

        print self.nrOfNonGons(), " non-gons in this problem"

        nonGonsPerGroup = self.nrOfNonGonsPerGroup(state)

        for group, nr in nonGonsPerGroup.items():
            print "group ", group, " has ", nr, " non-gons"

    def displayLevels(self, state):
        """Print levels stats to output
        """
        for level in settings.levels:
            print self.nrOfLevelXs(level), " of level ", level, " pupils in this problem"

            # Print the number of level X pupils in each group
            levelXsPerGroup = self.nrOfLevelXsPerGroup(level, state)

            for group, nr in levelXsPerGroup.items():
                print "group ", group, " has ", nr, " pupils of level ", level

    def displayDistribution(self, state):
        """Display each group and the pupils they contain in 'state'
        """
        namesPerGroup = {}

        for pupilId, groupName in state:
            if groupName not in namesPerGroup:
                namesPerGroup[groupName] = []
            parts = [
                self.pupils[pupilId].name,
                "(",
                str(self.countFriendsInGroup(pupilId, groupName, state)),
                "/",
                str(len(self.pupils[pupilId].friends)),
                ")"
            ]
            pupilAndFriends = "".join(parts)
            namesPerGroup[groupName].append(pupilAndFriends)

        for group, names in namesPerGroup.items():
            print "group ", group, " has ", len(names), " pupils: \n", ", \n".join(names)
            print "-" * 30

    def toString(self):
        """Return string representation of the problem"""
        pupilsAsString = ", ".join(map((lambda x: str(x[1].name)), self.pupils.items()))
        groupsAsString = ",".join(self.groups)
        return "Groups: " + groupsAsString + ", pupils: " + pupilsAsString


class Pupil:
    """Represents a Pupil"""
    def toString(self):
        return self.name